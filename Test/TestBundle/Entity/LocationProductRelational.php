<?php

// src/Test/TestBundle/Entity/LocationProductRelational.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * LocationProductRelational
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\LocationProductRelationalRepository")
 * @ORM\Table(name="LocationProductRelational")
 */
class LocationProductRelational	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $entryId;

	/**
     * @ORM\ManyToOne(targetEntity="Location")
	 * @ORM\JoinColumn(name="locationId", referencedColumnName="locationId", onDelete="CASCADE")
     */
    protected $locationId;
	
	/**
     * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="productId", referencedColumnName="productId", onDelete="CASCADE")
     */
    protected $productId;
	
	/**
	 * @ORM\Column(type="boolean")
	 */
	 protected $sellingCharacteristic;

    /**
     * Get entryId
     *
     * @return integer 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * Set sellingCharacteristic
     *
     * @param boolean $sellingCharacteristic
     * @return LocationProductRelational
     */
    public function setSellingCharacteristic($sellingCharacteristic)
    {
        $this->sellingCharacteristic = $sellingCharacteristic;

        return $this;
    }

    /**
     * Get sellingCharacteristic
     *
     * @return boolean 
     */
    public function getSellingCharacteristic()
    {
        return $this->sellingCharacteristic;
    }

    /**
     * Set locationId
     *
     * @param \Test\TestBundle\Entity\Location $locationId
     * @return LocationProductRelational
     */
    public function setLocationId(\Test\TestBundle\Entity\Location $locationId = null)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return \Test\TestBundle\Entity\Location 
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set productId
     *
     * @param \Test\TestBundle\Entity\Product $productId
     * @return LocationProductRelational
     */
    public function setProductId(\Test\TestBundle\Entity\Product $productId = null)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return \Test\TestBundle\Entity\Product 
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
