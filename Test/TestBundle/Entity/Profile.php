<?php

// src/Test/TestBundle/Entity/Location.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Profile
 *
 * @ORM\Entity
 * @ORM\Table(name="Profile")
 */
class Profile
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $profileId;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $Name;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $Barangay;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $City;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $Province;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $Region;

    /**
     * Get profileId
     *
     * @return integer 
     */
    public function getProfileId()
    {
        return $this->profileId;
    }

    /**
     * Set Name
     *
     * @param string $name
     * @return Profile
     */
    public function setName($name)
    {
        $this->Name = $name;

        return $this;
    }

    /**
     * Get Name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->Name;
    }

    /**
     * Set Barangay
     *
     * @param string $barangay
     * @return Profile
     */
    public function setBarangay($barangay)
    {
        $this->Barangay = $barangay;

        return $this;
    }

    /**
     * Get Barangay
     *
     * @return string 
     */
    public function getBarangay()
    {
        return $this->Barangay;
    }

    /**
     * Set City
     *
     * @param string $city
     * @return Profile
     */
    public function setCity($city)
    {
        $this->City = $city;

        return $this;
    }

    /**
     * Get City
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->City;
    }

    /**
     * Set Province
     *
     * @param string $province
     * @return Profile
     */
    public function setProvince($province)
    {
        $this->Province = $province;

        return $this;
    }

    /**
     * Get Province
     *
     * @return string 
     */
    public function getProvince()
    {
        return $this->Province;
    }

    /**
     * Set Region
     *
     * @param string $region
     * @return Profile
     */
    public function setRegion($region)
    {
        $this->Region = $region;

        return $this;
    }

    /**
     * Get Region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->Region;
    }
}
