<?php

namespace Test\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Test\TestBundle\Entity\Crop;

class SearchController extends Controller
{
    public function indexAction()
    {
		$em = $this
			->getDoctrine()
			->getManager();	// get the entity manager
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );	// get the crop repository (access to crop database)
		$productRepository = $em->getRepository( 'TestTestBundle:Product' ); // get the product repository (access to product database)
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' ); // get the location repository (access to location database)
		
		$crops = $cropRepository->findAllOrderedByName();	// get all objects from the crop repository (database)
		$products = $productRepository->findAllOrderedByName();	// get all objects from the product repository (database)
		$locations = $locationRepository->findAllOrderedByName(); // get all objects from the location repository (database)
	
		// render Search/index.html.twig passing along with variable $crops named as crops and $products named as products
        return $this->render( 'TestTestBundle:Search:index.html.twig',
			array(
				'crops' => $crops,
				'products' => $products,
				'locations' => $locations )
				);
    }
	
	// FUNCTION TO DISPLAY THE PROFILE OF THE SELECTED LINKWORD
	public function displayAction($id, $entity)
	{
		$searchResult = "ok";
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );
		$productRepository = $em->getRepository( 'TestTestBundle:Product' );
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$reasonRepository = $em->getRepository( 'TestTestBundle:Reason' );
		$cropProductRelationalRepository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
		$locationProductRelationalRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
		$reaLocProdRepo = $em->getRepository( 'TestTestBundle:ReaLocProdRelational' );
		$otherReaLocProdRepo = $em->getRepository( 'TestTestBundle:OtherReaLocProdRelational' );
		$result = array();
		$entryName = '';
		
		// CROP KEY
		if( $entity == "crop" )
		{
			$cropId = $id;
			$cropName = $cropRepository
				->findOneByCropId( $cropId )
				->getCropName();
			$productIds = array();
			$productNames = array();
			
			$cropProductRelations = $cropProductRelationalRepository->findByCropId( $cropId );
			foreach( $cropProductRelations as $cropProductRelation )
				$productIds[] = $cropProductRelation->getProductId();
			
			foreach( $productIds as $productId )
			{
				$productNames[] = $productRepository
					->findOneByProductId( $productId )
					->getProductName();
			}
			$result = $productNames;
			$entryName = $cropName;
			$searchResult = "cropSearch";
			return $this->render( 'TestTestBundle:Search:results.html.twig',
			array(
				'searchParameter' => $entryName,
				'arr' => $result,
				'searchResults' => $searchResult )
				);
		}
		
		// PRODUCT KEY
		elseif( $entity == "product" )
		{
			$productId = $id;
			$productName = $productRepository
				->findOneByProductId( $productId )
				->getProductName();
			$cropProdRelaIds = array();
			$locaProdRelaIds = array();
			$reaLocProdRelaIds = array();
			$otherReaLocProdRelaIds = array();
			
			$cropProdRelaIds = $cropProductRelationalRepository->findByProductId( $productId );
			
			$locaProdRelaIds = $locationProductRelationalRepository->findByProductId( $productId );
			foreach( $locaProdRelaIds as $locaProdRelaId )
			{
				$reaLocProdRelaIds[] = array( $reaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
				$otherReaLocProdRelaIds[] = array( $otherReaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
			}
			
			$entryName = $productName;
			$searchResult = "productSearch";
			
			return $this->render( 'TestTestBundle:Search:results.html.twig',
				array(
					'searchParameter' => $entryName,
					'reaLocProdRelaIds' => $reaLocProdRelaIds,
					'otherReaLocProdRelaIds' => $otherReaLocProdRelaIds,
					'cropProdRelaIds' => $cropProdRelaIds,
					'locaProdRelaIds' => $locaProdRelaIds,
					'searchResults' => $searchResult )
					);
		}
		
		// LOCATION KEY
		elseif( $entity == "location" )
		{
			$locationId = $id;
			$city = $locationRepository
				->findOneByLocationId( $locationId )
				->getCity();
			$province = $locationRepository
				->findOneByLocationId( $locationId )
				->getProvince();
			$cropProdRelaIds = array();
			$locaProdRelaIds = array();
			$reaLocProdRelaIds = array();
			$otherReaLocProdRelaIds = array();
			
			$locaProdRelaIds = $locationProductRelationalRepository->findByLocationId( $locationId );
			foreach( $locaProdRelaIds as $locaProdRelaId )
			{
				$reaLocProdRelaIds[] = array( $reaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
				$otherReaLocProdRelaIds[] = array( $otherReaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
			}
			
			$entryName = $city . ", " . $province;
			$searchResult = "locationSearch";
			
			return $this->render( 'TestTestBundle:Search:results.html.twig',
				array(
					'searchParameter' => $entryName,
					'reaLocProdRelaIds' => $reaLocProdRelaIds,
					'otherReaLocProdRelaIds' => $otherReaLocProdRelaIds,
					'cropProdRelaIds' => $cropProdRelaIds,
					'locaProdRelaIds' => $locaProdRelaIds,
					'searchResults' => $searchResult )
					);
		}
		
		// REASON KEY
		elseif( $entity == "reason" )
		{
			$reasonId = $id;
			$reasonName = $reasonRepository
				->findOneByReasonId( $reasonId )
				->getReasonName();
			$locaProdRelaIds = array();
			
			$reaLocProdRelations = $reaLocProdRepo->findByReasonId( $reasonId );
			foreach( $reaLocProdRelations as $reaLocProdRelation )
				$locaProdRelaIds[] = $reaLocProdRelation->getLocationProductId();
			
			$entryName = $reasonName;
			$searchResult = "reasonSearch";
			
			return $this->render( 'TestTestBundle:Search:results.html.twig',
				array(
					'searchParameter' => $entryName,
					'reaLocProds' => $reaLocProdRelations,
					'locaProdRelations' => $locaProdRelaIds,
					'searchResults' => $searchResult )
					);
		}		
	}
	
	// SEARCH FUNCTION
	public function searchAction()
	{
		$request = $this->get( 'request' );
		$entryId = $request->get( 'search_list' );
		$category = $request->get( 'categorySelected' );
		$searchResult = "ok";
		
		$em = $this
			->getDoctrine()
			->getManager();
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );
		$productRepository = $em->getRepository( 'TestTestBundle:Product' );
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$cropProductRelationalRepository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
		$locationProductRelationalRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
		$reaLocProdRepo = $em->getRepository( 'TestTestBundle:ReaLocProdRelational' );
		$otherReaLocProdRepo = $em->getRepository( 'TestTestBundle:OtherReaLocProdRelational' );
		$result = array();
		$entryName = '';
		if( $category == "keyword" )
		{
			$data = $request->get( 'searchKeyword' );
			$data = ucwords( strtolower( trim( $data, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			
			// CROP SEARCH QUERY BUILDER
			$qb = $em->createQueryBuilder();
			$qb->select(array('c'))
			   ->from('TestTestBundle:Crop', 'c')
			   ->where(
				   $qb->expr()->like('c.cropName', ':data')
			   )
			   ->setParameter('data', '%'.$data.'%');
			 $queryCrops = $qb->getQuery();
			 $temp = $queryCrops->getResult();
			 foreach($temp as $item)
			 {
				$result[] = array("id" => $item->getCropId(), "entity" => "crop", "data" => $item->getCropName());
			 }
			 
			// PRODUCT SEARCH QUERY BUILDER
			$qb2 = $em->createQueryBuilder();
			$qb2->select(array('p'))
			   ->from('TestTestBundle:Product', 'p')
			   ->where(
				   $qb2->expr()->like('p.productName', ':data')
			   )
			   ->setParameter('data', '%'.$data.'%');
			 $queryProducts = $qb2->getQuery(); 
			 $temp2 = $queryProducts->getResult();
			 foreach($temp2 as $item)
			 {
				$result[] = array("id" => $item->getProductId(), "entity" => "product", "data" => $item->getProductName());
			 }
			 
			// LOCATION SEARCH QUERY BUILDER
			$qb3 = $em->createQueryBuilder();
			$qb3->select(array('l'))
			   ->from('TestTestBundle:Location', 'l')
			   ->where($qb3->expr()->orx(
				   $qb3->expr()->like('l.city', ':data'),
				   $qb3->expr()->like('l.province', ':data'),
				   $qb3->expr()->like('l.region', ':data')
			   ))
			   ->setParameter('data', '%'.$data.'%');
			 $queryLocations = $qb3->getQuery();
			 $temp3 = $queryLocations->getResult();
			 
			 foreach($temp3 as $item)
			 {
				$result[] = array("id" => $item->getLocationId(), "entity" => "location", "data" => $item->getCity().", ".$item->getProvince());
			 }
			 
			// REASON SEARCH QUERY BUILDER
			$qb4 = $em->createQueryBuilder();
			$qb4->select(array('r'))
			   ->from('TestTestBundle:Reason', 'r')
			   ->where(
				   $qb4->expr()->like('r.reasonName', ':data')
			   )
			   ->setParameter('data', '%'.$data.'%');
			$queryReasons = $qb4->getQuery();
			$temp4 = $queryReasons->getResult();
			foreach($temp4 as $item)
			{
				$result[] = array("id" => $item->getReasonId(), "entity" => "reason", "data" => $item->getReasonName());
			}
			
			$entryName = $data;
			$searchResult = "keySearch";
			return $this->render( 'TestTestBundle:Search:results.html.twig',
			array(
				'searchParameter' => $entryName,
				'arr' => $result,
				'searchResults' => $searchResult )
				);
		}
		
		// CROP KEYWORD
		elseif( $category == "crop" )
		{
			$cropId = $entryId;
			$cropName = $cropRepository
				->findOneByCropId( $cropId )
				->getCropName();
			$productIds = array();
			$productNames = array();
			
			$cropProductRelations = $cropProductRelationalRepository->findByCropId( $cropId );
			foreach( $cropProductRelations as $cropProductRelation )
				$productIds[] = $cropProductRelation->getProductId();
			
			foreach( $productIds as $productId )
			{
				$productNames[] = $productRepository
					->findOneByProductId( $productId )
					->getProductName();
			}
			$result = $productNames;
			$entryName = $cropName;
			$searchResult = "cropSearch";
			return $this->render( 'TestTestBundle:Search:results.html.twig',
			array(
				'searchParameter' => $entryName,
				'arr' => $result,
				'searchResults' => $searchResult )
				);
		}
		
		// PRODUCT KEYWORD
		elseif( $category == "product" )
		{
			$productId = $entryId;
			$productName = $productRepository
				->findOneByProductId( $productId )
				->getProductName();
			$cropProdRelaIds = array();
			$locaProdRelaIds = array();
			$reaLocProdRelaIds = array();
			$otherReaLocProdRelaIds = array();
			
			$cropProdRelaIds = $cropProductRelationalRepository->findByProductId( $productId );
			
			$locaProdRelaIds = $locationProductRelationalRepository->findByProductId( $productId );
			foreach( $locaProdRelaIds as $locaProdRelaId )
			{
				$reaLocProdRelaIds[] = array( $reaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
				$otherReaLocProdRelaIds[] = array( $otherReaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
			}
			
			$entryName = $productName;
			$searchResult = "productSearch";
			
			return $this->render( 'TestTestBundle:Search:results.html.twig',
				array(
					'searchParameter' => $entryName,
					'reaLocProdRelaIds' => $reaLocProdRelaIds,
					'otherReaLocProdRelaIds' => $otherReaLocProdRelaIds,
					'cropProdRelaIds' => $cropProdRelaIds,
					'locaProdRelaIds' => $locaProdRelaIds,
					'searchResults' => $searchResult )
					);
		}
		
		// LOCATION KEYWORD
		elseif( $category == "location" )
		{
			$locationId = $entryId;
			$city = $locationRepository
				->findOneByLocationId( $locationId )
				->getCity();
			$province = $locationRepository
				->findOneByLocationId( $locationId )
				->getProvince();
			$cropProdRelaIds = array();
			$locaProdRelaIds = array();
			$reaLocProdRelaIds = array();
			$otherReaLocProdRelaIds = array();
			
			$locaProdRelaIds = $locationProductRelationalRepository->findByLocationId( $locationId );
			foreach( $locaProdRelaIds as $locaProdRelaId )
			{
				$reaLocProdRelaIds[] = array( $reaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
				$otherReaLocProdRelaIds[] = array( $otherReaLocProdRepo->findByLocationProductId( $locaProdRelaId->getEntryId() ) );
			}
			
			$entryName = $city . ", " . $province;
			$searchResult = "locationSearch";
			
			return $this->render( 'TestTestBundle:Search:results.html.twig',
				array(
					'searchParameter' => $entryName,
					'reaLocProdRelaIds' => $reaLocProdRelaIds,
					'otherReaLocProdRelaIds' => $otherReaLocProdRelaIds,
					'cropProdRelaIds' => $cropProdRelaIds,
					'locaProdRelaIds' => $locaProdRelaIds,
					'searchResults' => $searchResult )
					);
		}
	}
	
}