<?php

// src/Test/TestBundle/Entity/ReaLocProdRelational.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ReaLocProdRelational
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\ReaLocProdRelationalRepository")
 * @ORM\Table(name="ReaLocProdRelational")
 */
class ReaLocProdRelational	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $entryId;

	/**
     * @ORM\ManyToOne(targetEntity="Reason")
	 * @ORM\JoinColumn(name="reasonId", referencedColumnName="reasonId", onDelete="CASCADE")
     */
    protected $reasonId;
	
	/**
     * @ORM\ManyToOne(targetEntity="LocationProductRelational")
	 * @ORM\JoinColumn(name="locationProductId", referencedColumnName="entryId", onDelete="CASCADE")
     */
    protected $locationProductId;
	

    /**
     * Get entryId
     *
     * @return integer 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * Set reasonId
     *
     * @param \Test\TestBundle\Entity\Reason $reasonId
     * @return ReaLocProdRelational
     */
    public function setReasonId(\Test\TestBundle\Entity\Reason $reasonId = null)
    {
        $this->reasonId = $reasonId;

        return $this;
    }

    /**
     * Get reasonId
     *
     * @return \Test\TestBundle\Entity\Reason 
     */
    public function getReasonId()
    {
        return $this->reasonId;
    }

    /**
     * Set locationProductId
     *
     * @param \Test\TestBundle\Entity\LocationProductRelational $locationProductId
     * @return ReaLocProdRelational
     */
    public function setLocationProductId(\Test\TestBundle\Entity\LocationProductRelational $locationProductId = null)
    {
        $this->locationProductId = $locationProductId;

        return $this;
    }

    /**
     * Get locationProductId
     *
     * @return \Test\TestBundle\Entity\LocationProductRelational 
     */
    public function getLocationProductId()
    {
        return $this->locationProductId;
    }
}
