<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LocationRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->findBy(array(), array('region' => 'ASC', 'province' => 'ASC', 'city' => 'ASC', 'barangay' => 'ASC' ) );
    }
}
?>