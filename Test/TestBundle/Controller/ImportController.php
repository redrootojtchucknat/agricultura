<?php  

namespace Test\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Test\TestBundle\Entity\Location;
use Test\TestBundle\Entity\Farmer;

class ImportController extends Controller
{
	public function indexAction( $importResult = "fail" )
    {
		return $this->render( 'TestTestBundle:Import:index.html.twig',
			array(
				'importResult' => $importResult
				)
			);
    }

	public function importAction()
	{
	ini_set('max_execution_time', 0);
		$request = $this->get( 'request' );
		$csv = $request->files->get( 'csv' );
		$importResult = "fail";
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$farmerRepository = $em->getRepository( 'TestTestBundle:Farmer' );
		
		if (!empty($csv)) 
		{
			$handle = fopen($csv,"r"); 
			$data = array();
			//loop through the csv file and insert into database 
			while ($data = fgetcsv($handle,1000,"\t","'")) { 
				if ($data[0])
				{
					$farmer = new Farmer();
					$location = new Location();
					$name = $data[ 1 ];
					$barangay = $data[ 2 ];
					$city = $data[ 3 ];
					$province = $data[ 4 ];
					$region = $data[ 5 ];
					$name = ucwords( strtolower( trim( $name, $character_mask = " \t\n\r\0\x0B\"" ) ) );
					$barangay = ucwords( strtolower( trim( $barangay, $character_mask = " \t\n\r\0\x0B\"" ) ) );
					$city = ucwords( strtolower( trim( $city, $character_mask = " \t\n\r\0\x0B\"" ) ) );
					$province = ucwords( strtolower( trim( $province, $character_mask = " \t\n\r\0\x0B\"" ) ) );
					$region = strtoupper( trim( $region, $character_mask = " \t\n\r\0\x0B\"" ) );
					if( !empty( $barangay ) ) 
					{
						$location->setBarangay( $barangay );
					}
					$location->setCity( $city );
					$location->setProvince( $province );
					$location->setRegion( $region );
					// search if location exists in table
					$cityIds = array();
					$provinceIds = array();
					$regionIds = array();
					$existingCities = $locationRepository->findByCity( $city );
					$existingProvinces = $locationRepository->findByProvince( $province );
					$existingRegions = $locationRepository->findByRegion( $region );
					foreach( $existingCities as $city )
					{
						$cityIds[] = $city->getLocationId();
					}
					foreach( $existingProvinces as $province)
					{
						$provinceIds[] = $province->getLocationId();
					}
					foreach( $existingRegions as $region)
					{
						$regionIds[] = $region->getLocationId();
					}
					$intersection = array_intersect($cityIds, $provinceIds, $regionIds);
					if(empty($intersection)){
						$em->persist( $location );
						$em->flush();
						$locationId = $location->getLocationId();
						$farmer->setLocationId($location);
					}
					else{
						//add intersection to farmer's Location ID
						$locationInstance = $locationRepository->findOneByLocationId( reset( $intersection ) );
						$farmer->setLocationId( $locationInstance );
					}
				
					$farmer->setName( $name );
					$em->persist( $farmer );
				} 
			}
			$importResult = "success";
			$em->flush();
			
		}
		return $this->forward( 'TestTestBundle:Import:index',
			array(
				'importResult' => $importResult
				)
			);
	}
	 
}
?> 