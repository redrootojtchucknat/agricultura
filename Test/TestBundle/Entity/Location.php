<?php

// src/Test/TestBundle/Entity/Location.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Location
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\LocationRepository")
 * @ORM\Table(name="Location")
 */
class Location	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $locationId;
	
	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $barangay;
	
	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	protected $city;
	
	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	protected $province;
	
	/**
	 * @ORM\Column(type="text", nullable=false)
	 */
	protected $region;
	
	/**
	 * @ORM\Column(type="decimal", nullable=true)
	 */
	protected $latitude;
	
	/**
	 * @ORM\Column(type="decimal", nullable=true)
	 */
	protected $longitude;

    /**
     * Get locationId
     *
     * @return integer 
     */
    public function getLocationId()
    {
        return $this->locationId;
    }

    /**
     * Set barangay
     *
     * @param string $barangay
     * @return Location
     */
    public function setBarangay($barangay)
    {
        $this->barangay = $barangay;

        return $this;
    }

    /**
     * Get barangay
     *
     * @return string 
     */
    public function getBarangay()
    {
        return $this->barangay;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Location
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set province
     *
     * @param string $province
     * @return Location
     */
    public function setProvince($province)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Get province
     *
     * @return string 
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set region
     *
     * @param string $region
     * @return Location
     */
    public function setRegion($region)
    {
        $this->region = $region;

        return $this;
    }

    /**
     * Get region
     *
     * @return string 
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * Set latitude
     *
     * @param string $latitude
     * @return Location
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Get latitude
     *
     * @return string 
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Set longitude
     *
     * @param string $longitude
     * @return Location
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Get longitude
     *
     * @return string 
     */
    public function getLongitude()
    {
        return $this->longitude;
    }
}
