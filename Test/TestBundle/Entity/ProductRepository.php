<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ProductRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->findBy(array(), array('productName' => 'ASC'));
    }
}
?>