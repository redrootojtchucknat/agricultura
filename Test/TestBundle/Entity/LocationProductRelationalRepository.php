<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class LocationProductRelationalRepository extends EntityRepository
{
    public function findAllOrderedByLocationProduct()
    {
		$qb = $this->createQueryBuilder( 'lpr' );
		$qb->join( 'lpr.productId', 'p' )
			->join( 'lpr.locationId', 'l' )
			->addOrderBy( 'p.productName', 'ASC' )
			//->addOrderBy( 'l.region', 'ASC' )
			//->addOrderBy( 'l.province', 'ASC' )
			->addOrderBy( 'l.city', 'ASC' )
			->addOrderBy( 'l.barangay', 'ASC' );
		$query = $qb->getQuery();
		return $query->getResult();
    }
}
?>