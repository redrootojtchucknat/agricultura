<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CropProductRelationalRepository extends EntityRepository
{
    public function findAllOrderedByCropProduct()
    {
		$qb = $this->createQueryBuilder( 'cpr' );
		$qb->join( 'cpr.cropId', 'c' )
			->join( 'cpr.productId', 'p' )
			->addOrderBy( 'c.cropName', 'ASC' )
			->addOrderBy( 'p.productName', 'ASC' );
		$query = $qb->getQuery();
		return $query->getResult();
    }
}
?>