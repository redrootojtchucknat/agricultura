<?php

namespace Test\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class EditController extends Controller
{
	public function listAction( $editResult = "fail", $errorMessage = "" )
	{
		$em = $this
			->getDoctrine()
			->getManager();
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );	
		$productRepository = $em->getRepository( 'TestTestBundle:Product' ); 
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$reasonRepository = $em->getRepository( 'TestTestBundle:Reason' );
		$cropProductRepository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
		$locationProductRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
		$reaLocProdRepository = $em->getRepository( 'TestTestBundle:ReaLocProdRelational' );
		$otherReaLocProdRepository = $em->getRepository( 'TestTestBundle:OtherReaLocProdRelational' );
		$farmerRepository = $em->getRepository( 'TestTestBundle:Farmer' );
		
		$crops = $cropRepository->findAllOrderedByName();
		$products = $productRepository->findAllOrderedByName();
		$locations = $locationRepository->findAllOrderedByName();
		$reasons = $reasonRepository->findAllOrderedByName();
		$cropProducts = $cropProductRepository->findAllOrderedByCropProduct();
		$locationProducts = $locationProductRepository->findAllOrderedByLocationProduct();
		$reaLocProds = $reaLocProdRepository->findAll();
		$otherReaLocProds = $otherReaLocProdRepository->findAll();
		$farmers = $farmerRepository->findAllOrderedByName();
		
		ini_set( "memory_limit", "-1" );
	
		// render Search/list.html.twig passing along with variable $crops named as crops and $products named as products, etc.
        return $this->render( 'TestTestBundle:Edit:list.html.twig',
			array(
				'crops' => $crops,
				'products' => $products,
				'locations' => $locations,
				'reasons' => $reasons,
				'cropProducts' => $cropProducts,
				'locationProducts' => $locationProducts,
				'reaLocProds' => $reaLocProds,
				'otherReaLocProds' => $otherReaLocProds,
				'farmers' => $farmers,
				'editResult' => $editResult,
				'errorMessage' => $errorMessage )
				);
	
	}
	
	public function deleteAction(){
		
		$request = $this->get( 'request' );
		$entryId = $request->get( 'id' );
		$category = $request->get( 'categorySelected' );
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		if( $category == "crop" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Crop' );
			$crop = $repository->find($entryId);
			$em->remove($crop);
		}
		elseif( $category == "product" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Product' );
			$product = $repository->find($entryId);
			$em->remove($product);
		}
		elseif( $category == "location" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Location' );
			$location = $repository->find($entryId);
			$em->remove($location);
		}
		elseif( $category == "reasons" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Reason' );
			$reason = $repository->find($entryId);
			$em->remove($reason);
		}
		elseif( $category == "cropProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
			$cropProduct = $repository->find($entryId);
			$em->remove($cropProduct);
		}
		elseif( $category == "locationProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
			$locationProduct = $repository->find($entryId);
			$em->remove($locationProduct);
		}
		elseif( $category == "reasonLocationProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:ReaLocProdRelational' );
			$reaLocProd = $repository->find($entryId);
			$em->remove($reaLocProd);
		}
		elseif( $category == "otherReasonLocationProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:OtherReaLocProdRelational' );
			$otherReaLocProd = $repository->find($entryId);
			$em->remove($otherReaLocProd);
		}
		elseif( $category == "farmer" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Farmer' );
			$farmer = $repository->find($entryId);
			$em->remove($farmer);
		}
		$em->flush();
		return $this->forward( 'TestTestBundle:Edit:list' );
	}
	
	public function editAction( $editResult = "fail", $errorMessage = "" )
	{
		$request = $this->get( 'request' );
		$entryId = $request->get( 'id' );
		$category = $request->get( 'categorySelected' );
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		$entity = array();
		$editParameter = "";
		if( $category == "crop" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Crop' );
			$crop = $repository->find($entryId);
			$entity = $crop;
			$editParameter = "CROP";
			
		}
		elseif( $category == "product" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Product' );
			$product = $repository->find($entryId);
			$entity = $product;
			$editParameter = "PRODUCT";
		}
		elseif( $category == "location" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Location' );
			$location = $repository->find($entryId);
			$entity = $location;
			$editParameter = "LOCATION";
		}
		elseif( $category == "reason" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Reason' );
			$reason = $repository->find($entryId);
			$entity = $reason;
			$editParameter = "REASON";
		}
		/*
		elseif( $category == "cropProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
			$cropProduct = $repository->find($entryId);
		}
		elseif( $category == "locationProduct" )
		{
			$repository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
			$locationProduct = $repository->find($entryId);
		}
		*/
		elseif( $category == "farmer" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Farmer' );
			$farmer = $repository->find($entryId);
			$entity = $farmer;
			$editParameter = "FARMER";
		}
		
		return $this->render( 'TestTestBundle:Edit:editEntry.html.twig',
			array(
				'editParameter' => $editParameter,
				'entity' => $entity,
				'editResult' => $editResult,
				'errorMessage' => $errorMessage )
				);
	}
	
	public function updateAction()
	{
		$request = $this->get( 'request' );
		$validator = $this->get( 'validator' );
		$entryId = $request->get( 'id' );
		$category = $request->get( 'categorySelected' );
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		
		$entity = array();
		$editResult = "success";
		
		if( $category == "crop" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Crop' );
			$name = $request->get( 'entityName' );
			$name = ucwords( strtolower( trim( $name, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			
			$crop = $repository->find($entryId);
			$crop->setCropName( $name );
			$errors = $validator->validate( $crop );
			if( count( $errors ) > 0 )
			{
				$editResult = "fail";
				$em->detach( $crop );
			}
		}
		elseif( $category == "product" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Product' );
			$name = $request->get( 'entityName' );
			$name = ucwords( strtolower( trim( $name, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			
			$product = $repository->find($entryId);
			$product->setCropName( $name );
			$errors = $validator->validate( $product );
			if( count( $errors ) > 0 )
			{
				$editResult = "fail";
				$em->detach( $product );
			}
		}
		elseif( $category == "location" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Location' );
			$location = $repository->find($entryId);
			$barangay = $request->get( 'barangay' );
			$city = $request->get( 'city' );
			$province = $request->get( 'province' );
			$region = $request->get( 'region' );
			$barangay = ucwords( strtolower( trim( $barangay, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$city = ucwords( strtolower( trim( $city, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$province = ucwords( strtolower( trim( $province, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$region = strtoupper( trim( $region, $character_mask = " \t\n\r\0\x0B\"" ) );
			
			if( !empty( $barangay ) )
			{	
				$location->setBarangay( $barangay );
			}
			$location->setCity( $city );
			$location->setProvince( $province );
			$location->setRegion( $region );
			$errors = $validator->validate( $location );
			if( count( $errors ) > 0 )
			{
				$editResult = "fail";
				$em->detach( $location );
			}
			
		}
		elseif( $category == "reason" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Reason' );
			$name = $request->get( 'entityName' );
			$name = ucwords( strtolower( trim( $name, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			
			$reason = $repository->find($entryId);
			$reason->setReasonName( $name );
			$errors = $validator->validate( $reason );
			if( count( $errors ) > 0 )
			{
				$editResult = "fail";
				$em->detach( $reason );
			}
		}
		elseif( $category == "farmer" )
		{
			$repository = $em->getRepository( 'TestTestBundle:Farmer' );
			$farmer = $repository->find($entryId);
			$name = $request->get( 'entityName' );
			$name = ucwords( strtolower( trim( $name, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			
			$farmer->setName( $name );
			$errors = $validator->validate( $farmer );
			if( count( $errors ) > 0 )
			{
				$editResult = "fail";
				$em->detach( $farmer );
			}
		}
		
		$em->flush();
		if( strcmp( $editResult, "fail" ) )
		{
			return $this->forward( 'TestTestBundle:Edit:list',
				array
				(
					'editResult' => $editResult,
					'errorMessage' => $errors
				)
				);
		}
		else
		{
			return $this->forward( 'TestTestBundle:Edit:edit',
				array
				(
					'editResult' => $editResult,
					'errorMessage' => $errors
				)
				);
		}
	}
}