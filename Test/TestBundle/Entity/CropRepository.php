<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CropRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->findBy(array(), array('cropName' => 'ASC'));
    }
}
?>
