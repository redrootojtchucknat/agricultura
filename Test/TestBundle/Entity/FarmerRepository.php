<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class FarmerRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->findBy(array(), array('name' => 'ASC'));
    }
}
?>