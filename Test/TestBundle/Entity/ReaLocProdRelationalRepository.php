<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReaLocProdRelationalRepository extends EntityRepository
{
    public function findAllOrderedByProductLocationReason()
    {
		$qb = $this->createQueryBuilder( 'rlpr' );
		$qb->join( 'rlpr.locationProductId', 'lpr' )
			->join( 'lpr.locationId', 'l' )
			->join( 'lpr.productId', 'p' )
			->join( 'rlpr.reasonId', 'r' )
			->addOrderBy( 'p.productName', 'ASC' )
			->addOrderBy( 'l.region', 'ASC' )
			->addOrderBy( 'l.province', 'ASC' )
			->addOrderBy( 'l.city', 'ASC' )
			->addOrderBy( 'l.barangay', 'ASC' )
			->addOrderBy( 'r.reasonName', 'ASC' );
		$query = $qb->getQuery();
		return $query->getResult();
    }
}
?>
