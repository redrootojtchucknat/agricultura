<?php

namespace Test\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Test\TestBundle\Entity\Crop;
use Test\TestBundle\Entity\Product;
use Test\TestBundle\Entity\Location;
use Test\TestBundle\Entity\Farmer;
use Test\TestBundle\Entity\Reason;
use Test\TestBundle\Entity\CropProductRelational;
use Test\TestBundle\Entity\LocationProductRelational;
use Test\TestBundle\Entity\ReaLocProdRelational;
use Test\TestBundle\Entity\OtherReaLocProdRelational;

class AddController extends Controller
{
    public function indexAction( $addResult = "fail", $errorMessage = "", $successes = "" )
    {
		$em = $this
			->getDoctrine()
			->getManager();	// get the entity manager
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );	// get the crop repository (access to crop database)
		$productRepository = $em->getRepository( 'TestTestBundle:Product' ); // get the product repository (access to product database)
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' ); // get the location repository (access to location database)
		$reasonRepository = $em->getRepository( 'TestTestBundle:Reason' ); // get the reason repository (access to location database)
		$locationProductRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' ); // get the location-product repository (access to location database)
		
		$crops = $cropRepository->findAllOrderedByName();	// get all objects from the crop repository (database)
		$products = $productRepository->findAllOrderedByName();	// get all objects from the product repository (database)
		$locations = $locationRepository->findAllOrderedByName(); // get all objects from the location repository (database)
		$reasons = $reasonRepository->findAllOrderedByName(); // get all objects from the reason repository (database)
		$locProds = $locationProductRepository->findAllOrderedByLocationProduct(); // get all objects from the location-product repository (database)
			
		return $this->render( 'TestTestBundle:Add:index.html.twig',
			array(
				'addResult' => $addResult,
				'errorMessage' => $errorMessage,
				'crops' => $crops,
				'products' => $products,
				'locations' => $locations, 
				'reasons' => $reasons, 
				'successes' => $successes,
				'locProds' => $locProds )
				);
    }
	
	public function addAction()
	{
		$request = $this->get( 'request' );
		$validator = $this->get( 'validator' );
		$data = $request->get( 'entityName' );
		$category = $request->get( 'categorySelected' );
		
		
		$em = $this
			->getDoctrine()
			->getManager();
		$connection = $em->getConnection();
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );
		$productRepository = $em->getRepository( 'TestTestBundle:Product' );
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$cropProductRelationalRepository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
		$locationProductRelationalRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
		$reasonRepository = $em->getRepository( 'TestTestBundle:Reason' );
		$locationProductRepository = $em->getRepository( 'TestTestBundle:LocationProductReLational' ); 
		//$reaLocProdRepository = $em->getRepository( 'TestTestBundle:ReaLocProdReLational' ); 
		
		$farmerRepository = $em->getRepository( 'TestTestBundle:Farmer' );
		
		$data = ucwords( strtolower( trim( $data, $character_mask = " \t\n\r\0\x0B\"" ) ) );
		$addResult = "success";
		if( $category == "crop" )
		{
		// add crop here
			$crop = new Crop();
			$crop->setCropName( $data );
			$errors = $validator->validate( $crop );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $crop );
			}
		}
		elseif( $category == "product" )
		{
		// add product here
			$product = new Product();
			$product->setProductName( $data );
			$errors = $validator->validate( $product );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $product );
			}
		}
		elseif( $category == "location" )
		{
			$location = new Location();
			$barangay = $request->get( 'barangay' );
			$city = $request->get( 'city' );
			$province = $request->get( 'province' );
			$region = $request->get( 'region' );
			
			// format entry
			$barangay = ucwords( strtolower( trim( $barangay, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$city = ucwords( strtolower( trim( $city, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$province = ucwords( strtolower( trim( $province, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$region = strtoupper( trim( $region, $character_mask = " \t\n\r\0\x0B\"" ) );
			
			if( !empty( $barangay ) )
			{	
				$location->setBarangay( $barangay );
			}
			$location->setCity( $city );
			$location->setProvince( $province );
			$location->setRegion( $region );
			$errors = $validator->validate( $location );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $location );
			}
		}
		elseif( $category == "reason" )
		{
			$reason = new Reason();
			$reason->setReasonName( $data );
			$errors = $validator->validate( $reason );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $reason );
			}
		}
		elseif( $category == "locationProduct" )
		{
			$locationId = $request->get( 'locationList' );
			$productId = $request->get( 'productList' );
			$sellingCharacteristic = $request->get( 'sellingCharacteristic' );
			
			$locationInstance = $locationRepository->findOneByLocationId( $locationId );
			$productInstance = $productRepository->findOneByProductId( $productId );
			
			$locationProduct = new LocationProductRelational();
			$locationProduct->setLocationId( $locationInstance );
			$locationProduct->setProductId( $productInstance );
			$locationProduct->setSellingCharacteristic( $sellingCharacteristic );
			
			$errors = $validator->validate( $locationProduct );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $locationProduct );
			}
		}
		elseif( $category == "cropProduct" )
		{
			$cropId = $request->get( 'cropList' );
			$productId = $request->get( 'productList' );
			
			$cropInstance = $cropRepository->findOneByCropId( $cropId );
			$productInstance = $productRepository->findOneByProductId( $productId );
			
			$cropProduct = new CropProductRelational();
			$cropProduct->setCropId( $cropInstance );
			$cropProduct->setProductId( $productInstance );
			
			$errors = $validator->validate( $cropProduct );
			if( count( $errors ) > 0 )
			{
				$addResult = "fail";
			}
			else
			{
				$em->persist( $cropProduct );
			}
		}
		elseif( $category == "reasonLocationProduct" )
		{
			$locProdId = $request->get( 'locationProductList' );
			$reasons = $request->get( 'checkedReasons' );
			$locProdInstance = $locationProductRelationalRepository->findOneByEntryId( $locProdId );
			$otherReason = $request->get( 'otherReason' );
			
			$errors = array();
			$error = array();
			$success = array();
			if(!empty($reasons))
			{
				foreach($reasons as $reason)
				{
					$reasonInstance = $reasonRepository->findOneByReasonId( $reason );
					$reaLocProd = new ReaLocProdRelational();
					//$reaLocProd->setLocationId( $locationInstance );
					//$reaLocProd->setProductId( $productInstance );
					$reaLocProd->setReasonId( $reasonInstance );
					$reaLocProd->setLocationProductId( $locProdInstance );
					//implement array of reasons in checkedReasons[]
					//add to reaLocProd
					
					
					$error = $validator->validate( $reaLocProd );
					if( count( $error ) > 0 )
					{
						$addResult = "arrayOfFails";
						$errors[] = $error;
					}
					else
					{
						$success[] = $reasonInstance->getReasonName() . " successfully added!";
						$em->persist( $reaLocProd );
					}
				}
			}
			
			if(!empty($otherReason))
			{
				$otherReason = ucwords( strtolower( trim( $otherReason, $character_mask = " \t\n\r\0\x0B\"" ) ) );
				$otherReaLocProd = new OtherReaLocProdRelational();
				$otherReaLocProd->setOtherReasonName( $otherReason );
				$otherReaLocProd->setLocationProductId( $locProdInstance );
				$em->persist( $otherReaLocProd );
			}
			$em->flush();
			return $this->forward( 'TestTestBundle:Add:index',
			array
				(
				'addResult' => $addResult,
				'errorMessage' => $errors,
				'successes' => $success
				)
			);
		}
		
		elseif( $category == "farmer" )
		{
			$farmerName = $request->get( 'farmerName' );
			$locationId = $request->get( 'locationList' );
			
			$farmerName = ucwords( strtolower( trim( $farmerName, $character_mask = " \t\n\r\0\x0B\"" ) ) );
			$locationInstance = $locationRepository->findOneByLocationId( $locationId );
			//$farmer = $farmerRepository->findByName( $farmerName );
			//$locationInstance = $locationRepository->findByLocationId( $locationId );
			$farmer = new Farmer();
			$farmer->setName( $farmerName );
			$farmer->setLocationId( $locationInstance );
			
			$em->persist( $farmer );
		}
		
		$em->flush();
		
		return $this->forward( 'TestTestBundle:Add:index',
			array
				(
				'addResult' => $addResult,
				'errorMessage' => $errors
				)
			);
	}
}
