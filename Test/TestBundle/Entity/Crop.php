<?php

// src/Test/TestBundle/Entity/Crop.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Crop
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\CropRepository")
 * @ORM\Table(name="Crop")
 */
class Crop	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $cropId;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $cropName;

    /**
     * Get cropId
     *
     * @return integer 
     */
    public function getCropId()
    {
        return $this->cropId;
    }

    /**
     * Set cropName
     *
     * @param string $cropName
     * @return Crop
     */
    public function setCropName($cropName)
    {
        $this->cropName = $cropName;

        return $this;
    }

    /**
     * Get cropName
     *
     * @return string 
     */
    public function getCropName()
    {
        return $this->cropName;
    }
}
