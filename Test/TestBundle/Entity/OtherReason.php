<?php

// src/Test/TestBundle/Entity/OtherReason.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OtherReason
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\OtherReasonRepository")
 * @ORM\Table(name="OtherReason")
 */
class OtherReason	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $otherReasonId;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $otherReasonName;
	

    /**
     * Get otherReasonId
     *
     * @return integer 
     */
    public function getOtherReasonId()
    {
        return $this->otherReasonId;
    }

    /**
     * Set otherReasonName
     *
     * @param string $otherReasonName
     * @return OtherReason
     */
    public function setOtherReasonName($otherReasonName)
    {
        $this->otherReasonName = $otherReasonName;

        return $this;
    }

    /**
     * Get otherReasonName
     *
     * @return string 
     */
    public function getOtherReasonName()
    {
        return $this->otherReasonName;
    }
}
