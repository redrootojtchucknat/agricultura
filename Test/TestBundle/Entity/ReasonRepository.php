<?php

namespace Test\TestBundle\Entity;

use Doctrine\ORM\EntityRepository;

class ReasonRepository extends EntityRepository
{
    public function findAllOrderedByName()
    {
        return $this->findBy(array(), array('reasonName' => 'ASC'));
    }
}
?>