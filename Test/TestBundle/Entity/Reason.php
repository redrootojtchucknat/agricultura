<?php

// src/Test/TestBundle/Entity/Reason.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Reason
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\ReasonRepository")
 * @ORM\Table(name="Reason")
 */
class Reason	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $reasonId;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $reasonName;

    /**
     * Get reasonId
     *
     * @return integer 
     */
    public function getReasonId()
    {
        return $this->reasonId;
    }

    /**
     * Set reasonName
     *
     * @param string $reasonName
     * @return Reason
     */
    public function setReasonName($reasonName)
    {
        $this->reasonName = $reasonName;

        return $this;
    }

    /**
     * Get reasonName
     *
     * @return string 
     */
    public function getReasonName()
    {
        return $this->reasonName;
    }
}
