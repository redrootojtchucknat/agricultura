<?php

namespace Test\TestBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ViewController extends Controller
{
    public function indexAction()
	{
		$request = $this->get( 'request' );
		$em = $this
			->getDoctrine()
			->getManager();
			
		$cropRepository = $em->getRepository( 'TestTestBundle:Crop' );
		$productRepository = $em->getRepository( 'TestTestBundle:Product' );
		$locationRepository = $em->getRepository( 'TestTestBundle:Location' );
		$cropProductRelationalRepository = $em->getRepository( 'TestTestBundle:CropProductRelational' );
		$locationProductRelationalRepository = $em->getRepository( 'TestTestBundle:LocationProductRelational' );
		
		$crops = $cropRepository->findAllOrderedByName();
		$products = $productRepository->findAllOrderedByName();
		$locations = $locationRepository->findAllOrderedByName();
		$cropProducts = $cropProductRelationalRepository->findAllOrderedByCropProduct();
		$locaProducts = $locationProductRelationalRepository->findAllOrderedByLocationProduct();
		
		$definedLocations = array();
		foreach( $locaProducts as $locaProduct )
		{
			if( !( in_array( $locaProduct->getLocationId(), $definedLocations ) ) )
				$definedLocations[] = $locaProduct->getLocationId();
		}
		
		
		return $this->render( 'TestTestBundle:View:report.html.twig',
			array(
				'crops' => $crops,
				'products' => $products,
				'locations' => $locations,
				'cropProducts' => $cropProducts,
				'locaProducts' => $locaProducts,
				'definedLocations' => $definedLocations
				)
			);
	}
}