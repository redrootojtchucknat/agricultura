<?php

// src/Test/TestBundle/Entity/Farmer.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Farmer
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\FarmerRepository")
 * @ORM\Table(name="Farmer")
 */
class Farmer	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
	
	/**
	 * @ORM\Column(type="text")
	 */
	protected $name;
	
	/**
     * @ORM\ManyToOne(targetEntity="Location")
	 * @ORM\JoinColumn(name="locationId", referencedColumnName="locationId", onDelete="CASCADE")
     */
    protected $locationId;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Farmer
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set locationId
     *
     * @param \Test\TestBundle\Entity\Location $locationId
     * @return Farmer
     */
    public function setLocationId(\Test\TestBundle\Entity\Location $locationId = null)
    {
        $this->locationId = $locationId;

        return $this;
    }

    /**
     * Get locationId
     *
     * @return \Test\TestBundle\Entity\Location 
     */
    public function getLocationId()
    {
        return $this->locationId;
    }
}
