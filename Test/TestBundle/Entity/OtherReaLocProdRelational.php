<?php

// src/Test/TestBundle/Entity/OtherReaLocProdRelational.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * OtherReaLocProdRelational
 *
 * @ORM\Entity
 * @ORM\Table(name="OtherReaLocProdRelational")
 */
class OtherReaLocProdRelational	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $entryId;

	/**
	 * @ORM\Column(type="text")
	 */
	protected $otherReasonName;
	
	/**
     * @ORM\ManyToOne(targetEntity="LocationProductRelational")
	 * @ORM\JoinColumn(name="locationProductId", referencedColumnName="entryId", onDelete="CASCADE")
     */
    protected $locationProductId;
	

    /**
     * Get entryId
     *
     * @return integer 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }


    /**
     * Set locationProductId
     *
     * @param \Test\TestBundle\Entity\LocationProductRelational $locationProductId
     * @return OtherReaLocProdRelational
     */
    public function setLocationProductId(\Test\TestBundle\Entity\LocationProductRelational $locationProductId = null)
    {
        $this->locationProductId = $locationProductId;

        return $this;
    }

    /**
     * Get locationProductId
     *
     * @return \Test\TestBundle\Entity\LocationProductRelational 
     */
    public function getLocationProductId()
    {
        return $this->locationProductId;
    }

    /**
     * Set otherReasonName
     *
     * @param string $otherReasonName
     * @return OtherReaLocProdRelational
     */
    public function setOtherReasonName($otherReasonName)
    {
        $this->otherReasonName = $otherReasonName;

        return $this;
    }

    /**
     * Get otherReasonName
     *
     * @return string 
     */
    public function getOtherReasonName()
    {
        return $this->otherReasonName;
    }
}
