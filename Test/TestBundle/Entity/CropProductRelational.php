<?php

// src/Test/TestBundle/Entity/CropProductRelational.php
namespace Test\TestBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CropProductRelational
 *
 * @ORM\Entity(repositoryClass="Test\TestBundle\Entity\CropProductRelationalRepository")
 * @ORM\Table(name="CropProductRelational")
 */
class CropProductRelational	
{
	/**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
	protected $entryId;

	/**
     * @ORM\ManyToOne(targetEntity="Crop")
	 * @ORM\JoinColumn(name="cropId", referencedColumnName="cropId", onDelete="CASCADE")
     */
    protected $cropId;
	
	/**
     * @ORM\ManyToOne(targetEntity="Product")
	 * @ORM\JoinColumn(name="productId", referencedColumnName="productId", onDelete="CASCADE")
     */
    protected $productId;

    /**
     * Get entryId
     *
     * @return integer 
     */
    public function getEntryId()
    {
        return $this->entryId;
    }

    /**
     * Set cropId
     *
     * @param \Test\TestBundle\Entity\Crop $cropId
     * @return CropProductRelational
     */
    public function setCropId(\Test\TestBundle\Entity\Crop $cropId = null)
    {
        $this->cropId = $cropId;

        return $this;
    }

    /**
     * Get cropId
     *
     * @return \Test\TestBundle\Entity\Crop 
     */
    public function getCropId()
    {
        return $this->cropId;
    }

    /**
     * Set productId
     *
     * @param \Test\TestBundle\Entity\Product $productId
     * @return CropProductRelational
     */
    public function setProductId(\Test\TestBundle\Entity\Product $productId = null)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return \Test\TestBundle\Entity\Product 
     */
    public function getProductId()
    {
        return $this->productId;
    }
}
